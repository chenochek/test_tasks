/**
 * Отрисовать отладочную информацию кофемашины в element
 * @param debugInfo {CoffeeMachineDebugInfo} - отладочная информация
 * @param element {HTMLDivElement} - div с фиксированным размером 300x96 пикселей,
 *     в который будет отрисовываться баркод
 */
function renderBarcode(debugInfo, element) {
    // ваш код
    debugInfo["code"] = getFilledCode(debugInfo["code"]);
    debugInfo["message"] = getFilledMessage(debugInfo["message"]);
    let convertString = getStringForConvert(debugInfo),
        byteArray = convertStringToByteArr(convertString),
        binaryCodes = Array.from(`${getBinaryCodesStr(byteArray)}${getHashSumStr(byteArray)}`);
    element.style.display = 'flex';
    element.innerHTML = getStyleForElement();
    element.append(createDiv('content'));
    let contentDiv = document.querySelector(".content");
    contentDiv.append(createDiv('left-border'));
    contentDiv.append(createDiv('barcode-content'));
    contentDiv.append(createDiv('right-border'));
    let leftBorderEl = document.querySelector(".left-border"),
        rightBorderEl = document.querySelector(".right-border"),
        barcodeContentEl = document.querySelector(".barcode-content");
    appendBordersToElement(leftBorderEl);
    appendBordersToElement(rightBorderEl);
    appendSquare(barcodeContentEl, binaryCodes);

    function appendBordersToElement(element) {
        element.append(createDiv('line-black'));
        element.append(createDiv('line-white'));
        element.append(createDiv('line-black'));
        element.append(createDiv('line-white'));
        element.append(createDiv('line-black'));
    }

    function appendSquare(element, binaryArr) {
        binaryArr.forEach(el => el == "1"? element.append(createDiv('square-black')) : element.append(createDiv('square-white')));
    }

    function getFilledCode(code) {
        return parseInt(code.toString().padEnd(3, 0), 10);
    }

    function getFilledMessage(message) {
        return message.padEnd(34, ' ');
    }

    function getStringForConvert(debugInfo) {
        return `${debugInfo["id"]}${debugInfo["code"]}${debugInfo["message"]}`;
    }

    function convertStringToByteArr(str) {
        let convertArr = [];
        for(let i = 0; i < str.length; i++) {
            convertArr.push(str.charCodeAt(i));
        }
        return convertArr;

    }

    function getHashSumStr(arr) {
        const reducer = (accumulator, currentValue) => accumulator ^ currentValue;
        let sumStr = arr.reduce(reducer).toString(2);
        return getFilledToByteStr(sumStr);

    }

    function getFilledToByteStr(str) {
        return str.padStart(8, 0);
    }

    function getBinaryCodesStr(signByteArray) {
        let resultBinaryStr = "";
        signByteArray.forEach(element =>
            resultBinaryStr = `${resultBinaryStr}${getFilledToByteStr(element.toString(2))}`
        );
        return resultBinaryStr;
    }

    function getStyleForElement() {
        let style = `<style>

 .content {
      width: 300px;
      height: 96px;
      display: flex;
      flex-wrap: wrap;
    }
    
    .left-border {
    width: 22px;
    height: 96px;
        display: flex;
    flex-wrap: wrap;
    }
    
        .right-border {
         width: 22px;
    height: 96px;
        display: flex;
    flex-wrap: wrap;
    }
    
    .barcode-content {
    width: 256px;
    height: 96px;
    
    display: flex;
    flex-wrap: wrap;
    }
    
.line-black {
width: 4px;
height: 96px;
background-color: black;
}

.line-white {
width: 5px;
height: 96px;
background-color: white;
}

.square-black {
width: 8px;
height: 8px;
background-color: black;
}

.square-white {
width: 8px;
height: 8px;
background-color: white;
}
</style>`;

        return style;
    }

    function createDiv(className) {
        let element = document.createElement('div');
        element.className = className;
        return element;
    }
};

var elem = document.createElement("div");
document.body.append(elem);

(renderBarcode({
    "id": "Teapot1234",
    "code": 0,
    "message": "No coffee this is a teapot"
}, document.body))();