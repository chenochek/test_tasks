function solution(input) {
    // ...
    const ALIGN_LEFT = ':---',
          ALIGN_RIGHT = '---:',
          ALIGN_CENTER = ':---:',
          LEFT_DIVIDER = '| ',
          RIGHT_DIVIDER = ' | ',
          BOLD_TEXT = '**';
    document.body.innerHTML = input;
    let numberTd = 0,
        table = document.querySelector('table'),
        isColgroupExist = isGroupExist(table, 'colgroup'),
        isTheadExist = isGroupExist(table, 'thead'),
        isTbodyExist = isGroupExist(table, 'tbody'),
        alignStr = "",
        markdown = "";
     if(isColgroupExist) {
         alignStr = getAlignFromColgroup();
     }
     markdown += isTheadExist? getMarkdownHeader('thead') :  getMarkdownHeader('tbody');
     markdown += isColgroupExist? alignStr : getDefaultAlign(numberTd);
     markdown += getMarkdownContent();
    return markdown;

    function isGroupExist(element, nameGroup) {
        return element.querySelector(nameGroup)? true : false;
    }

    function getAlignFromColgroup() {
        let cols = document.querySelectorAll('col'),
            stringAlign = LEFT_DIVIDER;
        numberTd = cols.length;
        cols.forEach(element => {
            let alignValue = element.getAttribute('align')? defineAlignValue(element.getAttribute('align')) : ALIGN_LEFT;
            stringAlign += alignValue;
            stringAlign += RIGHT_DIVIDER;
        }  );
        stringAlign += '\n';
        return stringAlign;

        function defineAlignValue(alignValue) {
            let dictAlign = {
                "right" : ALIGN_RIGHT,
                "left": ALIGN_LEFT,
                "center": ALIGN_CENTER
            }

            return dictAlign[alignValue];
        }
    }

    function getMarkdownHeader(nameTag) {
        let element = document.querySelector(nameTag),
            tr = element.querySelector('tr'),
            columns = tr.children,
            headerStr = LEFT_DIVIDER;
        if(!isColgroupExist) {
            numberTd = columns.length;
        }
        for(let i = 0; i < numberTd; i++) {
            let textValue = getTextForTd(columns[i]);
            headerStr += `${textValue}${RIGHT_DIVIDER}`;
        }
        headerStr += '\n';
        return headerStr;

    }

    function getMarkdownContent() {
        let content = "";
        if(isTheadExist) {
            let trThead = document.querySelector('thead').children,
                numTrThead = trThead.length;
            if(numTrThead > 1) {
                for(let i = 1; i < numTrThead; i++ ) {
                    content += LEFT_DIVIDER;
                    let td = trThead[i].children;
                    for(let j = 0; j < numberTd; j++) {
                        content += `${getTextForTd(td[j])}${RIGHT_DIVIDER}`
                    }
                    content += '\n';
                }
            }
            let trTbody = document.querySelector('tbody').children;
            for(let i = 1; i < trTbody.length; i++ ) {
                content += LEFT_DIVIDER;
                let td = trTbody[i].children;
                for(let j = 0; j < numberTd; j++) {
                    content += `${getTextForTd(td[j])}${RIGHT_DIVIDER}`
                }
                content += '\n';
            }

        } else {
            let trTbody = document.querySelector('tbody').children,
                numTrTbody = trTbody.length;
            if(numTrTbody > 1) {
                for(let i = 1; i < numTrTbody; i++ ) {
                    content += LEFT_DIVIDER;
                    let td = trTbody[i].children;
                    for(let j = 0; j < numberTd; j++) {
                        content += `${getTextForTd(td[j])}${RIGHT_DIVIDER}`
                    }
                    content += '\n';
                }
            }
        }

        return content;
    }

    function getTextForTd(element) {
        let formatElementText = element.innerHTML.trim().replace('\n', '').replace(/[ ]{2,}/, ' ');
        return element.tagName == 'TH'? `${BOLD_TEXT}${formatElementText}${BOLD_TEXT}` : `${formatElementText}`
    }

    function getDefaultAlign(numberTd) {
        return `${LEFT_DIVIDER}${ALIGN_LEFT}`.repeat(numberTd) + `${RIGHT_DIVIDER}\n`;
    }

}

(console.log(solution(`<table>  
    <colgroup>  
        <col align="right" />  
        <col />  
        <col align="center" />  
    </colgroup>  
    <thead>  
        <tr>  
            <td>Command         </td>  
            <td>Description     </td>  
            <th>Is implemented  </th>  
        </tr>  
    </thead>  
    <tbody>  
        <tr>  
            <th>git status</th>  
            <td>List all new or modified    files</td>  
            <th>Yes</th>  
        </tr>  
        <tr>  
            <th>git diff</th>  
            <td>Show file differences that haven’t been  
 staged</td>  
            <td>No</td>  
        </tr>  
    </tbody>  
</table>`)));